# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import traceback

import click
from click.testing import CliRunner

from xmlhelpy import argument
from xmlhelpy import Bool
from xmlhelpy import Choice
from xmlhelpy import Float
from xmlhelpy import group
from xmlhelpy import Integer
from xmlhelpy import IntRange
from xmlhelpy import option
from xmlhelpy import Path


@group(name="my-group", version="1.0")
def example_group():
    """Example group.

    This docstring is only shown in the help when running the group directly.
    """


@example_group.command(
    name=None,
    description=None,
    example="1 --opt_a 1 -c a b -d tRUE",
    version="2.0",
)
@argument(
    "num",
    description="Some number >= 1 as string.",
    param_type=IntRange(min=1),
    as_string=True,
)
@argument(
    "outdir",
    description="Output directories.",
    param_type=Path(exists=False, path_type="dir"),
    default=("/", "/foo/b ar"),
    nargs=2,
)
@argument(
    "choice",
    description="Some choice.",
    required=True,
    param_type=Choice(["a", "b", "c"], case_sensitive=False),
    default="A",
)
@argument("outfiles", description="Output file names.", required=False, nargs=-1)
@option(
    "opt_a", char="a", description='Test option "A".', required=True, param_type=Float
)
@option(
    "opt_b",
    var_name="custom_opt_b",
    description='Test option "B".',
    is_flag=True,
    default="should_be_ignored",
    param_type=Integer,
    as_string=True,
)
@option(
    "opt_c",
    char="c",
    description='Test option "C".',
    default=("tes' t", "foo"),
    required=True,
    nargs=2,
)
@option("opt_d", char="d", description='Test option "D".', param_type=Bool)
def example_command(**kwargs):
    """Example command.

    This docstring is used as the description for the xmlhelp (if none was given) as
    well as in the help.
    """
    click.echo(kwargs)


def _invoke_runner(runner, params):
    result = runner.invoke(example_group, params)
    hashes = (len(str(params)) + 4) * "#"

    click.echo(hashes)
    click.echo(f"# {params} #")
    click.echo(hashes)
    click.echo(result.output)

    # Don't swallow exceptions while testing.
    if result.exception:
        click.echo("".join(traceback.format_tb(result.exception.__traceback__)))


if __name__ == "__main__":
    runner = CliRunner()

    _invoke_runner(runner, ["--commands"])
    _invoke_runner(runner, ["--version"])
    _invoke_runner(runner, ["example-command", "--version"])
    _invoke_runner(runner, ["example-command", "--help"])
    _invoke_runner(runner, ["example-command", "--xmlhelp"])
    _invoke_runner(
        runner, ["example-command", "1", "--opt_a", "1", "-c", "a", "b", "-d", "tRUE"]
    )
