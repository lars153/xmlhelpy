# Release history

## 0.4.0 (2021-03-23)

* Added the possibility to use custom command classes in decorators.
* Excluded empty examples in the generated XML.

## 0.3.0 (2021-02-25)

* Added example usage to help output.
* Added validation for duplicate parameter names and chars.

## 0.2.0 (2021-01-27)

* Added a `--commands` option to list all commands of a group.
* Added the option to pass a version to groups as well, which commands that do
  not specify their own version will inherit.
* Added a `--version` option to get the version of a group or command.

## 0.1.0 (2021-01-14)

* The basic `xmlhelp` interface is implemented.
