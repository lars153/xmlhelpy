# xmlhelpy

CLI wrapper library based on [Click](https://github.com/pallets/click) for
Python tools adding (part of) the *xmlhelp* interface and a few other helpful
options. The *xmlhelp* interface can be used to obtain a machine readable
representation of any command line tool and its parameters.

The following options are supported:

* `--xmlhelp`: The main option which implements the *xmlhelp* interface itself.
* `--version`: Prints the version of a tool, if specified.
* `--commands`: Prints a list of all subcommands a group command contains.

The library can be installed via pip by running `pip install xmlhelpy`. A usage
example can be found in `examples/example.py`.
