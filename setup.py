import os

from setuptools import find_packages
from setuptools import setup


with open(os.path.join("xmlhelpy", "version.py")) as f:
    exec(f.read())


with open("README.md") as f:
    long_description = f.read()


setup(
    name="xmlhelpy",
    version=__version__,
    license="Apache-2.0",
    author="Karlsruhe Institute of Technology",
    description="CLI wrapper for the xmlhelp interface.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/iam-cms/workflows/xmlhelpy",
    packages=find_packages(),
    include_package_data=True,
    python_requires=">=3.6",
    zip_safe=False,
    classifiers=[
        "Development Status :: 4 - Beta",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
    ],
    install_requires=[
        "Click>=7.0.0,<8.0.0",
        "lxml>=4.5.0,<5.0.0",
    ],
    extras_require={
        "dev": [
            "black==20.8b1",
            "pre-commit>=2.7.1",
            "pylint>=2.7.1,<2.8.0",
            "tox>=3.20.0",
        ]
    },
)
